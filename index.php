<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Transat Jacques Vabre 2007</title>
        <link rel="stylesheet" href="css/transat.css" />
    </head>

    <body>
        <div id="conteneur">
            <header>
                <div class='login'>
                    <form method="POST" action="">
                        <fieldset>
                            <legend>Connectez-vous</legend>
                            <input type="text" name="login" value="" placeholder="Login" required>
                            <input type="text" name="password" value="" placeholder="Password" required>
                            <input type="submit" value="Valider">
                        </fieldset>
                    </form>   
                </div>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php" class="navitem">Accueil</a></li>
                    <li><a href="classements.php" class="navitem">Classements</a></li>
                    <li><a href="insertclass.php" class="navitemprivate">Ajout Classe</a></li>
                </ul>
            </nav>
            <section>
                <article>                
                    <h1>Michel DESJOYEAUX vainqueur de la transat Jacques-Vabre 2007</h1>
                    <p>Le départ a été donné le 3 novembre 2007. Le parcours relie Le Havre à Salvador de bahia (Brésil).</p>
                    <p>Michel DESJOYEAUX, avec Emmanuel Le Borgne, sur Foncia (Classe Monocoque 60 IMOCA) ont gagné !</p>
                    <p><img src="images/vainqueur.jpg" /></p>
                </article>
            </section>


            <footer>
                <p>Copyright Moi - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>