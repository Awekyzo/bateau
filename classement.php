<?php $titre = "Le classement";?>

<?php
require ('bdd/bddconfig.php');
try {
$objbdd = new PDO("mysql:host=$bddserver;port=$bddport;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
$listeClasse = $objbdd ->query (" SELECT * FROM classebateau");


} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
    }
?>

<?php ob_start(); ?>
<article>
        <h1>Classement:</h1>
        <table>
        <thead>
            <tr>
                <th>Classes</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listeClasse as $class){ ?>
            <tr>
                <td><?php echo $class ['type'];?></td>
                <td><?php echo $class['nomClasse'];?></td>
            </tr>
            <?php
            }
            $listeClasse->closeCursor();
            ?>
        </tbody>
    </table>
</article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'template.php'; ?>